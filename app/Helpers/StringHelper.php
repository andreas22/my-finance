<?php namespace App\Helpers;

class StringHelper
{
    /**
     * Search in the given array against each key and return the matched
     *
     * @param array $sources
     * @param $term
     * @return array
     */
    public static function match(array $sources, $term)
    {
        $matches = [];

        foreach ($sources as $name => $value) {
            if (strpos(strtolower($name), strtolower($term)) !== false) {
                $matches[] = $value;
            }
        }

        return $matches;
    }
}