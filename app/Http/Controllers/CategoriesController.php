<?php namespace App\Http\Controllers;

use App\Models\Category;
use App\Repositories\CategoriesRepository;
use Illuminate\Http\Request;

class CategoriesController extends Controller {

    public function index()
    {
        return view('category.categories', ['categories' => Category::all()->sortBy('category_name')]);
    }

    /**
     * @param int $id
     * @param Request $request
     */
    public function save($id = 0, Request $request)
    {
        CategoriesRepository::save($id, $request->get('category_name'), $request->get('labels'));
    }

    /**
     * @param int $id
     */
    public function delete($id = 0)
    {
        CategoriesRepository::delete($id);
    }
}
