<?php namespace App\Http\Controllers;

use App\Helpers\StringHelper;
use App\Repositories\ConfigRepository;
use Illuminate\Http\Request;
use Input;

class ApiController extends Controller {

    public function getListOfSourcesThatMatch(Request $request)
    {
        $sources = ConfigRepository::getByType('source');

        return response()->json(StringHelper::match($sources, $request->get('term')));
    }
}

