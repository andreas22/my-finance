<?php namespace App\Repositories;

use App\Models\Category;

class CategoriesRepository {

    /**
     * Create or update a category and its labels
     *
     * @param int $id
     * @param $category_name
     * @param $labels
     * @return bool
     */
    public static function save($id = 0, $category_name, $labels)
    {
        $category = null;

        if($id > 0)
        {
            $category = Category::find($id);
        }

        if(is_null($category))
        {
            $category = new Category();
        }

        $category->category_name = $category_name;
        $category->labels = $labels;

        return $category->save();
    }

    /**
     * Delete a category
     *
     * @param $id
     */
    public static function delete($id)
    {
        Category::find($id)->delete();
    }
}